/**
 * Globe.js
 * --------
 * A javascript library based on Three.js for Globe rendering
 * --------
 * v1.0.0
 * 22/may/2021
 * Pierre-Loup Gosse
 * GPL Licence
 */

const __POSITION_COUNTRY = {
  ad: [42.5, 1.5],
  ae: [24, 54],
  af: [33, 65],
  al: [41, 20],
  am: [40, 45],
  an: [12.25, -68.75],
  ao: [-12.5, 18.5],
  ap: [35, 105],
  aq: [-90, 0],
  ar: [-34, -64],
  at: [47.3333, 13.3333],
  au: [-27, 133],
  aw: [12.5, -69.9667],
  az: [40.5, 47.5],
  ba: [44, 18],
  bd: [24, 90],
  be: [50.8333, 4],
  bf: [13, -2],
  bg: [43, 25],
  bh: [26, 50.55],
  bi: [-3.5, 30],
  bj: [9.5, 2.25],
  bn: [4.5, 114.6667],
  bo: [-17, -65],
  br: [-10, -55],
  bt: [27.5, 90.5],
  bv: [-54.4333, 3.4],
  bw: [-22, 24],
  bz: [17.25, -88.75],
  ca: [54, -100],
  cd: [0, 25],
  cf: [7, 21],
  ch: [47, 8],
  cl: [-30, -71],
  cm: [6, 12],
  cn: [35, 105],
  co: [4, -72],
  cr: [10, -84],
  cy: [35, 33],
  cz: [49.75, 15.5],
  de: [51, 9],
  dj: [11.5, 43],
  dk: [56, 10],
  do: [19, -70.6667],
  dz: [28, 3],
  ec: [-2, -77.5],
  ee: [59, 26],
  eg: [27, 30],
  eh: [24.5, -13],
  er: [15, 39],
  es: [40, -4],
  et: [8, 38],
  eu: [47, 8],
  fi: [64, 26],
  fo: [62, -7],
  fr: [46, 2],
  ga: [-1, 11.75],
  gb: [54, -2],
  ge: [42, 43.5],
  gf: [4, -53],
  gh: [8, -2],
  gi: [36.1833, -5.3667],
  gl: [72, -40],
  gm: [13.4667, -16.5667],
  gn: [11, -10],
  gq: [2, 10],
  gr: [39, 22],
  gt: [15.5, -90.25],
  gw: [12, -15],
  gy: [5, -59],
  hk: [22.25, 114.1667],
  hn: [15, -86.5],
  hr: [45.1667, 15.5],
  ht: [19, -72.4167],
  hu: [47, 20],
  id: [-5, 120],
  ie: [53, -8],
  il: [31.5, 34.75],
  in: [20, 77],
  is: [65, -18],
  it: [42.8333, 12.8333],
  jm: [18.25, -77.5],
  jo: [31, 36],
  jp: [36, 138],
  ke: [1, 38],
  kg: [41, 75],
  kh: [13, 105],
  kr: [37, 127.5],
  kw: [29.3375, 47.6581],
  kz: [48, 68],
  la: [18, 105],
  lb: [33.8333, 35.8333],
  li: [47.1667, 9.5333],
  lk: [7, 81],
  ls: [-29.5, 28.5],
  lt: [56, 24],
  lu: [49.75, 6.1667],
  lv: [57, 25],
  ly: [25, 17],
  ma: [32, -5],
  mc: [43.7333, 7.4],
  md: [47, 29],
  me: [42, 19],
  mg: [-20, 47],
  mk: [41.8333, 22],
  ml: [17, -4],
  mn: [46, 105],
  mo: [22.1667, 113.55],
  mr: [20, -12],
  mw: [-13.5, 34],
  mx: [23, -102],
  my: [2.5, 112.5],
  mz: [-18.25, 35],
  na: [-22, 17],
  ne: [16, 8],
  ng: [10, 8],
  ni: [13, -85],
  nl: [52.5, 5.75],
  no: [62, 10],
  np: [28, 84],
  nz: [-41, 174],
  om: [21, 57],
  pa: [9, -80],
  pe: [-10, -76],
  pg: [-6, 147],
  ph: [13, 122],
  pk: [30, 70],
  pl: [52, 20],
  pm: [46.8333, -56.3333],
  ps: [32, 35.25],
  pt: [39.5, -8],
  py: [-23, -58],
  qa: [25.5, 51.25],
  ro: [46, 25],
  rs: [44, 21],
  ru: [60, 100],
  rw: [-2, 30],
  sa: [25, 45],
  se: [62, 15],
  sg: [1.3667, 103.8],
  si: [46, 15],
  sj: [78, 20],
  sk: [48.6667, 19.5],
  sl: [8.5, -11.5],
  sm: [43.7667, 12.4167],
  sn: [14, -14],
  so: [10, 49],
  sr: [4, -56],
  sv: [13.8333, -88.9167],
  sz: [-26.5, 31.5],
  td: [15, 19],
  tg: [8, 1.1667],
  th: [15, 100],
  tj: [39, 71],
  tn: [34, 9],
  tr: [39, 35],
  tt: [11, -61],
  tw: [23.5, 121],
  tz: [-6, 35],
  ua: [49, 32],
  ug: [1, 32],
  us: [38, -97],
  uy: [-33, -56],
  uz: [41, 64],
  va: [41.9, 12.45],
  ve: [8, -66],
  vn: [16, 106],
  ye: [15, 48],
  za: [-29, 24],
  zm: [-15, 30],
};

const __W_WIDTH = window.innerWidth;
const __W_HEIGHT = window.innerHeight;
const __CANVAS = document.body;

const __RES_PATH = '/globe/res';
const __RES_MAP_NAME = 'map';

let   __FONT_ON_LOAD = true;
const __FONTS = {
  monospace: {
    path: 'source_code_pro_regular',
    res: undefined,
  },
};

const __GLOBE_RADIUS = 800;
const __GLOBE_DOT_RADIUS = 2.5;
const __GLOBE_DOT_SEG = 6;
const __GLOBE_DOT_VERTICES = __GLOBE_DOT_SEG * 3;
const __GLOBE_DOT_NB = 150000;
const __GLOBE_CIRCLE_SEG = 16;

const __MATERIAL_IDX_GLOBE_DOT = 0;

const __PI = Math.PI;
const __DEG_TO_RAD = __PI / 180;
const __RAD_TO_DEG = 180 / __PI;

const __ERR = -1;
const __ERR_NO_MAP = -2;
const __ERR_NO_COLOR = -3;
const __ERR_EXIST = -4;
const __ERR_NOT_EXIST = -5;
const __ERR_IDX = -6;
const __ERR_ARGS = -7;
const __ERR_FONT_UNLOADED = -8;

const __CLAMP = (num, min, max) => {
  return num <= min ? min : (num >= max ? max : num);
}


class Materials {
  
  static list = [];
  static hash = {};

  static key(args) {
    if (args.materialKey) 
      return args.materialKey;

    if (args.materialLayer)
      return `${args.materialLayer}@
        ${args.color}:${(args.opacity ? args.opacity : 1)}`;

    return `${args.color}:${(args.opacity ? args.opacity : 1)}`;
  }

  static add(args) {
    return this.addAt(this.list.length, args);
  }

  static addAt(index, args) {
    if (index < 0)
      return __ERR_IDX;
    // always a color for a material
    if (args.color === undefined) 
      return __ERR_NO_COLOR;
    // error if material already exists
    const key = this.key(args);
    if (this.hash[key] !== undefined) 
      return __ERR_EXIST;
    // avoid Three.js warning, moreover it is useless properties at this point
    delete args.materialKey;
    delete args.materialLayer;
    args.transparent = args.opacity !== undefined;
    const material = new THREE.MeshBasicMaterial(args);
    this.list[index] = material;
    this.hash[key] = index;
    material.materialId = index;

    return material;
  }

  static removeLayer(layer) {
    for (let key in this.hash) {
      const i = key.indexOf('@');
      if (i < 0) 
        continue;

      if (key.substring(0, i) == layer) {
        const index = this.hash[key];
        this.list[index] = undefined;
        this.hash[key] = undefined; 
      }
    }
  }

  static fetch(args) {
    const key = this.key(args);
    const value = this.hash[key];
    // material already exists
    if (value !== undefined) {
      if (this.list[value] === undefined)
        return __ERR_IDX;
      
      return this.list[value];
    }
    // create the material and return it
    else {
      return this.add(args);
    }
  }
}

class Position {

  constructor(lat, lng, args = {}) {
    this.lat = lat;
    this.lng = lng;
    this.elevation = (args.elevation !== undefined ? args.elevation : 0);
    this.radius = __GLOBE_RADIUS + this.elevation;
    this.offsetX = (args.offsetX !== undefined ? args.offsetX : 0);
    this.offsetY = (args.offsetY !== undefined ? args.offsetY : 0);
    const o = Position.sphericalToVector(lat, lng, this.radius);
    this.origin = new Vector(o[0], o[1], o[2]);
    const v = Position.sphericalToVector(lat + this.offsetY, lng + this.offsetX, 
      this.radius + this.elevation);
    this.vector = new Vector(v[0], v[1], v[2]);
  }
  
  static rotateX(vector, deg) {
    deg = deg * __DEG_TO_RAD;
    const x = vector.x;
    const y = (vector.y * Math.cos(deg)) - (vector.z * Math.sin(deg));
    const z = (vector.y * Math.sin(deg)) + (vector.z * Math.cos(deg));
    
    vector.set(x, y, z);
  }

  static rotateY(vector, deg) {
    deg = deg * __DEG_TO_RAD;
    const x = (vector.x * Math.cos(deg)) + (vector.z * Math.sin(deg));
    const y = vector.y;
    const z = (-vector.x * Math.sin(deg)) + (vector.z * Math.cos(deg));
    
    vector.set(x, y, z);
  }

  static rotateZ(vector, deg) {
    deg = deg * __DEG_TO_RAD;
    const x = (vector.x * Math.cos(deg)) - (vector.y * Math.sin(deg));
    const y = (vector.x * Math.sin(deg)) + (vector.y * Math.cos(deg));
    const z = vector.z;
    
    vector.set(x, y, z);
  }

  static sphericalToVector(lat, lng, radius = __GLOBE_RADIUS){
    const phi   = (90.5 - lat) * __DEG_TO_RAD;
    const theta = (lng + 78.5) * __DEG_TO_RAD;
  
    const x = (radius * (Math.sin(phi) * Math.cos(theta)));
    const y = (radius * Math.cos(phi));
    const z = (radius * Math.sin(phi) * Math.sin(theta));
  
    return [-x, y, z];
  }

  static vectorToSpherical(x, y, z) {
    const radius = Math.sqrt(-x * -x + y * y + z * z);
    const phi    = 90.5 - ((Math.atan2(Math.sqrt(-x * -x + z * z), y))
      * __RAD_TO_DEG);
    const theta  = (Math.atan2(z, -x) *__RAD_TO_DEG) - 78.5;

    return [radius, phi, theta];
  } 
}

class Marker {

  constructor(args) {
    if (args.position === undefined)
      return __ERR_ARGS;

    if (args.color === undefined)
      return __ERR_ARGS;

    const group = new THREE.Group();
    const markerPosition = Globe.findNearDot(args.position);
    const marker = new THREE.CircleGeometry(__GLOBE_DOT_RADIUS, 
      __GLOBE_DOT_SEG);
    const markerMaterial = Materials.fetch({color: args.color});
    Geometry.setAt(marker, markerPosition);
    const markerMesh = new THREE.Mesh(marker, markerMaterial);
    group.add(markerMesh);

    if (args.halo !== undefined) {
      const haloSize = (args.halo.size !== undefined ? args.halo.size : 5);
      const haloPosition = markerPosition;
      const halo = new THREE.CircleGeometry(__GLOBE_DOT_RADIUS * haloSize, 
        __GLOBE_CIRCLE_SEG);
      const haloMaterial = Materials.fetch({
        color: (args.halo.color !== undefined ? args.halo.color : args.color),
        opacity: (args.halo.opacity !== undefined ? args.halo.opacity : 0.25),
      });
      Geometry.setAt(halo, haloPosition);
      const haloMesh = new THREE.Mesh(halo, haloMaterial);
      group.add(haloMesh);
    } 

    if (args.label !== undefined) {
      args.label.position = new Position(markerPosition.lat, markerPosition.lng,
        args.label.position);
      group.add(new Label(args.label));
    }

    return group;
  }
}

class Label {

  constructor(args) {
    if (__FONT_ON_LOAD)
      return __ERR_FONT_UNLOADED;

    if (args.position === undefined)
      return __ERR_ARGS;

    if (args.text === undefined)
      return __ERR_ARGS;

    if (args.color === undefined)
      return __ERR_ARGS;

    if (args.font === undefined)
      args.font = __FONTS.monospace.res;

    if (args.size === undefined)
      args.size = 16;

    if (args.height === undefined)
      args.height = 1;

    if (args.rotation === undefined)
      args.rotation = 0;

    const group = new THREE.Group();
    const letterMaterial = Materials.fetch({color: args.color});
    const phi = (args.position.lat * __DEG_TO_RAD);
    let letterInfos, letterGeometry, nbChar, ratio, charSize, totalWidth;
    let positionVector = args.position.vector;

    Position.rotateX(positionVector, args.rotation);

    letterInfos = args.text.split('').map((letter) => {
      letterGeometry = new THREE.TextBufferGeometry(letter, {
        font: args.font,
        size: args.size,
        height: args.height,
        curveSegments: 16
      });
      if (letterGeometry)
        Geometry.setAt(letterGeometry, positionVector);
      
      return letterGeometry ? 
        new THREE.Mesh(letterGeometry, letterMaterial) : null; 
    });

    nbChar = letterInfos.length;
    ratio = Math.abs((Math.tan(Math.abs(phi) - __PI))) * __DEG_TO_RAD;
    charSize = 0.02 + ratio;
    totalWidth = - ((nbChar * charSize) / 2) + 0.002;
    for (const mesh of letterInfos) {
      if (mesh) {
        group.add(mesh);
        mesh.rotation.y = totalWidth;
      }
      totalWidth += charSize;
    }
    
    return group;
  }
}

class Line {

  constructor(args) {
    const group = new THREE.Group();
    const start = args.from.vector;
    const end = args.to.vector;

    let altitude = __CLAMP(start.distanceTo(end) * 0.75, 20, 200);
    
    const interpolate = d3.geoInterpolate([args.from.lng, args.from.lat], [args.to.lng, args.to.lat]);
    const midCoord1 = interpolate(0.25);
    const midCoord2 = interpolate(0.75);
    const labelCoord = interpolate(0.50);

    const mid1 = new Position(midCoord1[0], midCoord1[1], {elevation: altitude});
    const mid2 = new Position(midCoord2[0], midCoord2[1], {elevation: altitude});
    const label = new Position(labelCoord[0], labelCoord[1], {elevation: altitude});

    const curve = new THREE.CubicBezierCurve3(args.from.vector, mid1.vector, mid2.vector, args.to.vector);
    const points = curve.getPoints( 50 );
    const geometry = new THREE.BufferGeometry().setFromPoints( points );
    const material = new THREE.LineBasicMaterial( { color : 0xff0000 } );
    const curveObject = new THREE.Line(geometry, material);

    group.add(new Label({
      position: label,
      text: "prout",
      color: "red",
    }));

    group.add(curveObject);

    return group;
  }
}

class UV extends THREE.Vector2 {

  toUV(vector) {
    const v = vector.normalize(),
          x = 1 - (0.5 + Math.atan2(v.x, v.z) / (2 * __PI)),
          y = 0.5 + Math.asin(v.y) / __PI;
    this.x = -x;
    this.y = -y;
  }
}

class Vector extends THREE.Vector3 {
  
  constructor(x, y, z, radius, lat, lng) {
    super(x, y, z);
    this.radius = radius;
    this.lat = lat;
    this.lng = lng;
  }

  setFromSphericalCoords(radius, phi, theta) {
    super.setFromSphericalCoords(radius, phi, theta);
    const p = Position.vectorToSpherical(this.x, this.y, this.z);
    this.radius = radius;
    this.lat = p[1];
    this.lng = p[2];
  }

  copy(v) {
    super.copy(v);
    this.radius = v.radius;
    this.lat = v.lat;
    this.lng = v.lng;

    return this;
  }
}

class Geometry {

  static computeCircleVertices(segments, positions) {
    const vertices = [];
    let center = [positions[0], positions[1], positions[2]];
    let a, b;
    a = [positions[3], positions[4], positions[5]];
    for (let i = 6; i <= (segments+1) * 3; i += 3) {
      b = [positions[i], positions[i + 1], positions[i + 2]];
      vertices.push(...center, ...a, ...b);
      a = b;
    }
    return vertices;
  }

  static setAt(object, vector) {
    object.lookAt(vector);
    object.translate(vector.x, vector.y, vector.z);
  }
}

class Sample {

  constructor(r, g, b, a) {
    this.r = r;
    this.g = g;
    this.b = b;
    this.a = a;
    this.activated = (a == 255);
    this.colored = ((r != 0 || g != 0 || b != 0) && a == 255);
    this.empty = (r == 0 && g == 0 && b == 0 && a == 255);
    this.rgb = `rgb(${this.r}, ${this.g}, ${this.b})`;
    this.rgba = `rgba(${this.r}, ${this.g}, ${this.b}, ${this.a})`;
  }
}

class Ressources {

  static loadFont(font, callback) {
    new THREE.FontLoader().load(`${__RES_PATH}/font/${font}.json`, 
      callback);
  }

  static loadMap(map, callback) {
    new THREE.ImageLoader().load(`${__RES_PATH}/map/${map}.png`, 
      callback);
  }

  static loadLayer(layer, callback) {
    new THREE.ImageLoader().load(`${__RES_PATH}/layer/${layer}.png`, 
      callback);
  }

  static readImageData(res) {
    return (function (t) {
      const i = t.width,
            e = t.height,
            s = document.createElement("canvas");
            (s.width = i), (s.height = e);
      const a = s.getContext("2d");
      return a.drawImage(t, 0, 0), a.getImageData(0, 0, i, e);
    })(res);
  }

  static sampleImage(uv, data) {
    const w = data.width,
          h = data.height,
          i = 4 * Math.floor(uv.x * w) + Math.floor(uv.y * h) * (4 * w);
    data = data.data.slice(i, i + 4);
    return new Sample(data[0], data[1], data[2], data[3]);
  }
}

/**
 * Globe class
 * 
 * Entry point of the library. Create the scene and the rendering. Provides
 * functions to manipulate the globe: add layer, marker, label, arrow, etc.
 * 
 * @class
 */
class Globe {

  constructor() {
    this.init();
    this.loadFonts();
    this.addRenderer();
    this.addCamera();
    this.addGlobe();
    this.render();
    setTimeout(() => {
      this.addMap(__RES_MAP_NAME);
    }, 50);
  }

  init() {
    this.scene = new THREE.Scene();
    this.layers = [];
  }

  loadFonts() {
    let nbFonts = 0, loadedFonts = 0;

    for (const font in __FONTS) {
      nbFonts++;
      const path = __FONTS[font].path;
      Ressources.loadFont(path, (res) => {
        __FONTS[font].res = res;
        loadedFonts++;
        if (loadedFonts >= nbFonts)
          __FONT_ON_LOAD = false;
      });
    }
  }

  /**
   * Create the renderer and add it to the HTML
   */
  addRenderer() {
    this.renderer = new THREE.WebGLRenderer({
      antialias: true
    });
    this.renderer.setSize(__W_WIDTH, __W_HEIGHT);
    // background color
    this.renderer.setClearColor(0x0);
    // adds the 3D renderer to the HTML
    __CANVAS.appendChild(this.renderer.domElement);
  }

  addObject(object) {
    this.scene.add(object);
  } 
  
  /**
   * Add camera and orbit controls
   */
  addCamera() {
    const fov = 70;
    const aspect = __W_WIDTH / __W_HEIGHT;
    const near = 0.1;
    const far = 2250;

    this.camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
    this.camera.position.z = 1500;
    // add orbit controls
    new THREE.OrbitControls(this.camera, this.renderer.domElement);
  }
  
  /**
   * Add the globe
   * 
   * The globe is represented with a sphere
   */
  addGlobe() {
    const segWidth = 32;
    const segHeight = 32;
    const geometry = new THREE.SphereGeometry(__GLOBE_RADIUS, segWidth, 
      segHeight);
    const material = new THREE.MeshBasicMaterial({
      color: 0x1b1b1b,
      wireframe: true
    });

    this.sphere = new THREE.Mesh(geometry, material);
    this.addObject(this.sphere);
    // set the dot material
    Materials.addAt(__MATERIAL_IDX_GLOBE_DOT, {
      materialKey: 'globe_dot',
      color: 0x2e2e2e,
    });
  }

  buildMap(mapData) {
    const vector = new Vector();
    const uv = new UV();
    const dotTemplate = new THREE.CircleGeometry(__GLOBE_DOT_RADIUS,
      __GLOBE_DOT_SEG);
    const dot = new THREE.BufferGeometry();
    const vertices = [];
    let phi, theta;
    let d, sample, nbDots = 0;

    this.mapData = mapData;
    this.globeDots = new THREE.BufferGeometry();

    for (d = 0; d < __GLOBE_DOT_NB; d++) {
      phi = Math.acos(-1 + (2 * d) / __GLOBE_DOT_NB);
      theta = Math.sqrt(__GLOBE_DOT_NB * __PI) * phi;
      vector.setFromSphericalCoords(__GLOBE_RADIUS, phi, theta);
      dot.copy(dotTemplate);
      Geometry.setAt(dot, vector);
      dot.computeBoundingSphere();
      uv.toUV(vector);
      sample = Ressources.sampleImage(uv, mapData);
      if (sample.activated) {
        vertices.push(...Geometry.computeCircleVertices(__GLOBE_DOT_SEG,
          dot.attributes.position.array));
        nbDots++;
      }
    }

    this.globeDots.addGroup(0, __GLOBE_DOT_VERTICES * nbDots,
      __MATERIAL_IDX_GLOBE_DOT);
    this.globeDots.setAttribute('position', 
      new THREE.Float32BufferAttribute(vertices, 3));
    this.addObject(new THREE.Mesh(this.globeDots, Materials.list));
  }

  addMap(map) {
    Ressources.loadMap(map, (res) => {
      this.buildMap(Ressources.readImageData(res));
    });
  }

  buildLayer(layerData) {
    if (this.mapData === undefined) 
      return __ERR_NO_MAP;

    const vector = new Vector();
    const uv = new UV();
    const startGroup = this.globeDots.groups.length;
    const globeDots = this.globeDots;
    let phi, theta;
    let sampleMap, sampleLayer;
    let material, prevMaterial;
    let d, groupedDots, groups = 0, dotIndex = 0;

    for (d = 0; d < __GLOBE_DOT_NB; d++) {
      phi = Math.acos(-1 + (2 * d) / __GLOBE_DOT_NB);
      theta = Math.sqrt(__GLOBE_DOT_NB * __PI) * phi;
      vector.setFromSphericalCoords(__GLOBE_RADIUS, phi, theta);
      uv.toUV(vector);
      sampleMap = Ressources.sampleImage(uv, this.mapData);
      if (sampleMap.activated) {
        sampleLayer = Ressources.sampleImage(uv, layerData);
        if (sampleLayer.colored) {
          groupedDots++;
          material = Materials.fetch({
            materialLayer: layerData.name,
            color: sampleLayer.rgb,
          })
          if (!prevMaterial) {
            groupedDots = 0;
          }
          else if (prevMaterial.materialId != material.materialId) {
            addGroup((dotIndex - groupedDots) * __GLOBE_DOT_VERTICES,
              groupedDots * __GLOBE_DOT_VERTICES, prevMaterial.materialId);
          }
          prevMaterial = material;
        }
        else if (prevMaterial !== undefined && sampleLayer.empty) {
          addGroup((dotIndex - groupedDots - 1) * __GLOBE_DOT_VERTICES,
            (groupedDots + 1) * __GLOBE_DOT_VERTICES, prevMaterial.materialId);
          prevMaterial = undefined;
        }
        dotIndex++;
      }
    }

    function addGroup(start, count, materialId) {
      globeDots.addGroup(start, count, materialId);
      groupedDots = 0;
      groups++;
    }

    return {start: startGroup, count: groups};
  }

  addLayer(layer) {
    if (this.layers[layer] !== undefined)
      return __ERR_EXIST;

    Ressources.loadLayer(layer, (res) => {
      const data = Ressources.readImageData(res);
      data.name = layer;
      const range = this.buildLayer(data);
      this.layers[layer] = range;
    });
  }

  removeLayer(layer) {
    const range = this.layers[layer];
    if (range === undefined)
      return __ERR_NOT_EXIST;
    
    this.globeDots.groups.splice(range.start, range.count);

    for (let layer in this.layers) {
      layer = this.layers[layer];
      if (layer.start > range.start)
        layer.start -= range.count;
    }

    Materials.removeLayer(layer);
    delete this.layers[layer];
  }

  bringFrontLayer(layer) {
    if (this.layers[layer] === undefined)
      return __ERR_NO_EXIST;
    
    this.removeLayer(layer);
    this.addLayer(layer);
  }

  addLabelObject(args) {
    this.addObject(new Label(args));
  }

  addLabel(position, text, color) {
    this.addLabelObject({position, text, color});
  }

  addMakerObject(args) {
    this.addObject(new Marker(args));
  }

  addMarker(position, color, text, textOffset, textColor) {
    this.addMakerObject({
      position, color,
      halo: {
        
      },
      label: (text === undefined ? text : {
        position: {
          elevation: 1,
          offsetX: (textOffset !== undefined ? textOffset.x : undefined),
          offsetY: (textOffset !== undefined ? textOffset.y : undefined),
        },
        text,
        color: (textColor !== undefined ? textColor : color),
      }),
    });
  }

  addLineObject(args) {
    this.addObject(new Line(args));
  }

  addLine(from, to, color, marker, text) {
    this.addLineObject({
      from, to,

    })
  }

  /**
   * Render routine
   */
  render() {
    requestAnimationFrame(() => {
      this.render();
      this.renderer.render(this.scene, this.camera);
    });
  }


  static findNearDot(position) {
    const vector = new Vector();
    let nearVector = new Vector();
    let d, phi, theta, distance, minDistance = Infinity;

    for (d = 0; d < __GLOBE_DOT_NB; d++) {
      phi = Math.acos(-1 + (2 * d) / __GLOBE_DOT_NB);
      theta = Math.sqrt(__GLOBE_DOT_NB * __PI) * phi;
      vector.setFromSphericalCoords(__GLOBE_RADIUS, phi, theta);
      distance = position.vector.distanceTo(vector);
      if (distance < 6 && distance < minDistance) {
        minDistance = distance;
        nearVector.copy(vector);
      }
    }

    return nearVector;
  }
}

const g = new Globe();